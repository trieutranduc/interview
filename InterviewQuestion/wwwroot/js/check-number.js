﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () {
    $('#btn-submit').on('click', function () {
        var number = $('#ip-number').val();
        var model =[];
        number.split(',').map(function (item) {
                model.push(item);
        });
         
        $.ajax({
            url: '/Home/SortNumber',
            method: 'POST',
            data: { model: model},
            success: function (e) {
              
                $('#result-sort').text(e);
                $('#result').show();
            },
            error: function (e) {

            }
        });

    });
});

