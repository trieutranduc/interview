﻿using InterviewQuestion.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace InterviewQuestion.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SortNumber(List<string> model)
        {
            var rg = @"\d";
            Regex regex = new Regex(rg);
           
            var valid = model.Where(x=> !regex.IsMatch(x)).FirstOrDefault();
            if (!string.IsNullOrEmpty(valid) || model.Count < 25)
            {
                return new JsonResult("Bạn đã nhập sai dữ liệu.");
            }
            var lstNumber = model.Select(s => int.Parse(s)).OrderByDescending(x => x).ToList();

            // List<int> lstNumber = new List<int>() { 10, 9, 3, 1, 100, 4, 101, 121, 5, 130, 140, 100, 105, 0, 11, 2, 3, 4, 78, 98, 46, 50, 67, 66, 68 };
            var maxNumber = lstNumber.Max();
            var countNumber = lstNumber.Count();

            var getMaxValue = lstNumber.Where(x => x <= maxNumber).Select(x => x).Skip(0).Take(10).ToList();
            var getNextValue = lstNumber.Where(x => x <= maxNumber).Select(x => x).Skip(10).Take(10).ToList();
            var getMinValue = lstNumber.Select(x => x).Skip(20).Take(countNumber - 20).ToList();

            getMinValue.AddRange(getMaxValue);
            getMinValue.AddRange(getNextValue);

            return new JsonResult(getMinValue);
        }
    }
}